# gocan

#### 介绍
gocan库
功能有：
log:兼容几个常用的标准库log函数 方便在开发的时候使用log 然后生产环境替换这个库可以控制不显示日志
conf:默认要配合项目根目录conf.toml
cache:简单的缓存功能带过期 非线性安全

#### 安装教程
引入库 带后面指定功能 如要用log就用
```
"gitee.com/jieyang/gocan/log"
```

#### 使用说明
log:(主要方便在开发的时候使用log.Println 然后生产环境替换这个库设置成非调试模式可以控制不显示日志)
```
SetMode("debug") //设置为其他的就不是debug模式
F //同Fatalf
Fatal //同Fatalf
Fatalf //同Fatalf
Println //同Println 只debug模式显示 带[DEBUG]前缀
P //带[PSLOG]前缀
E //带[ERROR]前缀
T //带[TRACE]前缀 只debug模式显示
```
conf:
```
Appdir() //获取程序根目录 (暂时不能用 请参考代码丢到main.go里使用)
Load()    //重新加载根目录conf.toml 因为默认加载了一遍 可以不用
Get(key)    //获取conf.toml配置文件里的项 如只有一级的用 Get("appname") 二级用 Get("mysql.host")
//注:conf.toml里默认server.debug为false就会联动log开启非debug模式
```
conf.toml 示例二级配置代码

```
#toml配置文件
[server]
debug = false
[mysql]
host = "127.0.0.1:3306"
user = "root"
pass = "123456"
```
cache:

```
Set("key","value",60) //设置key的值为value过期时间60秒 第三个参数为过期时间秒 设置为-1为永不过期 重复设置值会覆盖
Get("key") //获取key的值 没有会返回nil
```





