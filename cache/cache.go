package cache

import (
	"time"
)

type Cache struct {
	
}

type cacher struct {
	cache	interface{}
	t		int //到期倒计时 秒
}

var cachemap map[string]cacher

func init(){
	// 初始化map
	cachemap = make(map[string]cacher)
	go timeout()
}

func timeout(){
	t := time.NewTicker(time.Second)
	for {
		select {
			case <-t.C:
			for k,v := range cachemap {
				if v.t>0 {
					v.t = v.t - 1
				}
				cachemap[k] = v
				if v.t==0 {
					delete(cachemap,k)
				}
			}
		}
	}
}
//
func Set(k string,v interface{},t int) {
	cachemap[k] = cacher {
		cache:v,
		t:t,
	}
}

func Get(k string) interface{}{
	return cachemap[k].cache
}

func Del(k string){
	delete(cachemap,k)
}