package conf

import (
	"gitee.com/jieyang/gocan/log"
	"path/filepath"
	"os"
	"github.com/pelletier/go-toml"
)

var Data *toml.Tree
var appdir string
// 获取当前执行文件绝对路径（go run）
func Appdir() string {
	return appdir
}

func Fdir(f string) string {
	fdir := f
	dir, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	_, err := os.Stat(fdir)    //os.Stat获取文件信息
	if os.IsNotExist(err) {
		fdir = dir+"/" + f
	}
	return fdir
}

func Load() {
	configfile := Fdir("conf.toml")
	config, err := toml.LoadFile(configfile) //加载toml文件
	if err != nil {
		log.F(configfile + "加载失败!",err)
	}
	if  config.Get("server.debug") !=nil && !config.Get("server.debug").(bool) {
		log.SetMode("release")
	}
	Data = config
}

func init(){
	Load()
	if dir2()=="." {
		appdir =  dir1()
	}else{
		appdir =  dir3()
	}
}

func Get(k string) interface{}{
	return Data.Get(k)
}

func dir1() string{
	dir, _ := os.Getwd()
	return dir
}

func dir2() string {
	return filepath.Dir(Fdir("conf.toml"))
}

func dir3() string {
	path,_ := os.Executable()
	return filepath.Dir(path)
}