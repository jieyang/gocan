package log

import (
	"log"
)
var mode = "debug"

func SetMode(m string) {
	mode = m
}

func F(v string, err error){
	log.Fatalf("[ERROR] "+v,err)
}

func Fatal(err error){
	log.Fatalf("[ERROR] %s",err)
}

func Fatalf(v string, err error){
	log.Fatalf("[ERROR] "+v,err)
}

func Println(v interface{},args ...interface{}) {
	if mode=="debug" {
		if len(args)>0 {
			log.Println("[DEBUG]",v,args)
		}else{
			log.Println("[DEBUG]",v)
		}
	}
}

func P(v interface{},args ...interface{}) {
	if len(args)>0 {
		log.Println("[PSLOG]",v,args)
	}else{
		log.Println("[PSLOG]",v)
	}
}
func E(v interface{},args ...interface{}) {
	if len(args)>0 {
		log.Println("[ERROR]",v,args)
	}else{
		log.Println("[ERROR]",v)
	}
}

func T(v interface{},args ...interface{}) {
	if mode=="debug" {
		if len(args)>0 {
			log.Println("[TRACE]",v,args)
		}else{
			log.Println("[TRACE]",v)
		}
	}
}